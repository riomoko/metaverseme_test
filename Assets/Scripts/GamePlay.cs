﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GamePlay : MonoBehaviour
{
    private static GamePlay _instance;
    public static GamePlay Instance
    { 
        get
        {
            if (_instance == null)
            {
                var go = new GameObject("GamePlay");
                _instance = go.AddComponent<GamePlay>();
            }
            return _instance;
        }
    }
    
    public BallController Ball;
    public PlayerController Player;

    public Text ScoreLabel;
    public Text LivesLabel;
    public Text GetReadyLabel;

    public uint Score = 0;
    public uint Lives = 3;

    uint Briks = 4;

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            DestroyImmediate(_instance.gameObject);
            _instance = this;
        }
        
        Reset();
        if (GetReadyLabel != null)
        {
            StartCoroutine(StartGame());
        }
    }

    void ScoreUpdate()
    {
        ScoreLabel.text = GamePlay.Instance.Score.ToString();
        LivesLabel.text = GamePlay.Instance.Lives.ToString();  
    }

    private void Reset()
    {
        Score = 0;
        Lives = 3;
        Briks = 4;
    }

    private IEnumerator StartGame()
    {
        GetReadyLabel.enabled = true;
        Player.Reset();
        Ball.Reset();
        ScoreUpdate();
        yield return new WaitForSeconds(3f);
        GetReadyLabel.enabled = false;
        Ball.Kick();
    }

    private void Update()
    {
#if true //debug commands
        if (Input.GetKeyDown(KeyCode.Q))
        {
            Lives = 0;
        }
        else if (Input.GetKeyDown(KeyCode.W))
        {
            Score = Briks;
        }
#endif
    }

    public void Death()
    {
        Lives--;
        ScoreUpdate();
        if (Lives == 0)
        {
            SceneManager.LoadScene("Lose");
        }
        else
        {
            StartCoroutine(StartGame()); 
        }
    }

    public void BrickCollision()
    {
       Score++;
       ScoreUpdate();
       if (Score == Briks)
       {
           SceneManager.LoadScene("Win");
       }
    }
}