﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class BallController : MonoBehaviour
{
    public float Speed = 1f;
    private Rigidbody2D rb;

    private void Awake()
    {
         rb = GetComponent < Rigidbody2D>();
    }

    public void Kick()
    {
        rb.velocity = Random.insideUnitCircle * Speed;
        // TO DO control on unwanted directions
    }

    private void FixedUpdate()
    {
        rb.velocity = rb.velocity.normalized * Speed;
    }

    public void Reset()
    {
        transform.position = Vector3.zero;
        rb.velocity = Vector2.zero;
    }
}