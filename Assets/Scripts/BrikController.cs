﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrikController : MonoBehaviour
{    
    private void OnCollisionEnter2D(Collision2D other)
    {
        GamePlay.Instance.BrickCollision();
        Destroy(gameObject);
    }
}