This project contains a simple Arkanoid/Breakout clone. It's been written quite poorly,
contains a number of bad Unity coding practices and has gameplay bugs.

Your mission is to identify and fix these problems, and generally make the project less shonky.

Please only take a few hours max on this - you won't score higher by including parallax starfields,
funky graphics or sfx - the purpose of the task is to see if you can identify the problems,
and how you reorganise things to make a more stable, meaningful and extendible project.

NB: The project has been designed with a 9:16 ratio in portrait mode.